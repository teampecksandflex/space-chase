#ifndef _bullet_H                 // Prevent multiple definitions if this 
#define _bullet_H                 // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"

namespace bulletNS
{
    const int WIDTH = 16;                   // image width
    const int HEIGHT = 16;                  // image height
    const int X = 0;
    const int Y = 0;
    const int   TEXTURE_COLS = 0;
}

// inherits from Entity class
class Bullet : public Entity
{
public:
    // constructor
    Bullet();

	int index;

    // inherited member functions
    virtual void draw();
    virtual bool initialize(Game *gamePtr, int width, int height, int ncols,
                            TextureManager *textureM);
    void update(float frameTime, float shotVelocity);
	void spawn(int x, int y);

	bool inUse;
};
#endif
