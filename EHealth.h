#ifndef _EHealth_H                 // Prevent multiple definitions if this 
#define _EHealth_H                 // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"

namespace EHealthNS
{
    const int WIDTH = 128;                   // image width
    const int HEIGHT = 64;                  // image height
    const int X = 1000;
    const int Y = 10;
    const int   TEXTURE_COLS = 1;          
}

// inherits from Entity class
class EHealth : public Entity
{


public:
    // constructor
    EHealth();
	float startTime;
    // inherited member functions
    virtual void draw();
    virtual bool initialize(Game *gamePtr, int width, int height, int ncols,
                            TextureManager *textureM);
    void update(float frameTime);

};
#endif