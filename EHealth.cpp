#include "EHealth.h"

//=============================================================================
// default constructor
//=============================================================================
EHealth::EHealth() : Entity()
{
    spriteData.width = EHealthNS::WIDTH;           // size of Ship1
    spriteData.height = EHealthNS::HEIGHT;
    spriteData.x = EHealthNS::X;                   // location on screen
    spriteData.y = EHealthNS::Y;
    spriteData.rect.bottom = EHealthNS::HEIGHT;    // rectangle to select parts of an image
    spriteData.rect.right = EHealthNS::WIDTH;
    radius = EHealthNS::WIDTH/2.0;
    collisionType = entityNS::CIRCLE;
}

//=============================================================================
// Initialize the Ship.
// Post: returns true if successful, false if failed
//=============================================================================
bool EHealth::initialize(Game *gamePtr, int width, int height, int ncols,
    TextureManager *textureM)
{
    return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

//=============================================================================
// draw the ship
//=============================================================================
void EHealth::draw()
{
    Image::draw();              // draw ship
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void EHealth::update(float frameTime)
{
    Entity::update(frameTime);
}