// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 ship.cpp v1.0

#include "playerShip.h"

//=============================================================================
// default constructor
//=============================================================================
playerShip::playerShip() : Entity()
{
    spriteData.width = playerShipNS::WIDTH;           // size of Ship1
    spriteData.height = playerShipNS::HEIGHT;
    spriteData.x = playerShipNS::X;                   // location on screen
    spriteData.y = playerShipNS::Y;
    spriteData.rect.bottom = playerShipNS::HEIGHT;    // rectangle to select parts of an image
    spriteData.rect.right = playerShipNS::WIDTH;
    radius = playerShipNS::WIDTH/2.0;
    collisionType = entityNS::CIRCLE;
	health=SHIP_INITIAL_HEALTH;
}

//=============================================================================
// Initialize the Ship.
// Post: returns true if successful, false if failed
//=============================================================================
bool playerShip::initialize(Game *gamePtr, int width, int height, int ncols,
    TextureManager *textureM)
{
    return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

//=============================================================================
// draw the ship
//=============================================================================
void playerShip::draw()
{
    Image::draw();              // draw ship
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void playerShip::update(float frameTime)
{
    Entity::update(frameTime);
}

int playerShip::getHealth()
{
	return health;
}

void playerShip::reduceHealth()
{
	health -= 1;
}
