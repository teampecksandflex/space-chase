#include "bullet.h"

//=============================================================================
// default constructor
//=============================================================================
Bullet::Bullet() : Entity()
{
    spriteData.width = bulletNS::WIDTH;           // size of Ship1
    spriteData.height = bulletNS::HEIGHT;
    spriteData.x = bulletNS::X;                   // location on screen
    spriteData.y = bulletNS::Y;
    spriteData.rect.bottom = bulletNS::HEIGHT;    // rectangle to select parts of an image
    spriteData.rect.right = bulletNS::WIDTH;
    velocity.x = 0;                             // velocity X
    velocity.y = 0;                             // velocity Y
    //radius = bulletNS::WIDTH/2.0;
	collisionType = entityNS::BOX;
	//rotationRate = 0;
	inUse = false;
}

//=============================================================================
// Initialize the Ship.
// Post: returns true if successful, false if failed
//=============================================================================
bool Bullet::initialize(Game *gamePtr, int width, int height, int ncols,
    TextureManager *textureM)
{
    return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

//=============================================================================
// draw the ship
//=============================================================================
void Bullet::draw()
{
    Image::draw();              // draw bullet
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void Bullet::update(float frameTime, float shotVelocity)
{
    Entity::update(frameTime);
    spriteData.x += frameTime * shotVelocity;         // move bullet along X
	//if(spriteData.x<-spriteData.width) inUse=false;
	//spriteData.angle+=rotationRate*frameTime;
}

void Bullet::spawn(int x, int y){
	spriteData.x=x;
	spriteData.y=y;
	//rotationRate=((rand()%5)-2.5)/2.0;
	inUse=true;
}