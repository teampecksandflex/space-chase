#ifndef _ESHIP_H                 // Prevent multiple definitions if this 
#define _ESHIP_H                 // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"


namespace enemyShipNS
{
    const int WIDTH = 128;                   // image width
    const int HEIGHT = 64;                  // image height
    const int X = ESHIP_INITIAL_X;
    const int Y = GAME_HEIGHT/4 - HEIGHT/4;
    const int   TEXTURE_COLS = 1;          
}

enum state{OFFSCREEN,ENTERING_SCREEN,ONSCREEN,DAMAGED,LEAVING_SCREEN,DESTROYED};

// inherits from Entity class
class enemyShip : public Entity
{
private:
	int health;
	float timer;
	bool shielded;
	int damageJitter;
	state aiState;
public:
    // constructor
    enemyShip();
	state getState();
	bool isShielded();
	void setShielded(bool s);
	void setState(state s);
	int direction;
    // inherited member functions
    virtual void draw();
    virtual bool initialize(Game *gamePtr, int width, int height, int ncols,
                            TextureManager *textureM);
    void update(float frameTime);

	int getHealth();
	void reduceHealth();
};
#endif