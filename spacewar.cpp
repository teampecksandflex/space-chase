// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Draw animated spaceships with collision and shield
// Chapter 6 spacewar.cpp v1.0
// This class is the core of the game

#include "spacewar.h"
#include "segment.h"
#include<iostream>
#include<fstream>
#include<string>
#include<random>
#include <ctime>

//=============================================================================
// Constructor
//=============================================================================
Spacewar::Spacewar()
{
	//Segment 0: no asteroids, lanes free
	levelVelocity=450;
	bulletFired=false;
	eShipScreenTimer=0;
	gameOver = false;
	isWon = false;
	//gameTimer = 0.0;
	numHits = 0;

	timerFont = new TextDX();
	finalTimeFont = new TextDX();

	int segCount=0;
	int asteroidCount=0;
	string tName;
	bool tLanes[NUM_SEGMENT_LANES];
	int tWidth;

	ifstream fin("segments.txt");
	fin>>segCount;
	
	for(int i=0;i<segCount;i++){
		segment temp;
		fin>>tName;
		temp.setName(tName);
		for(int j=0;j<NUM_SEGMENT_LANES;j++) fin>>tLanes[j];
		temp.setLanes(tLanes);
		fin>>tWidth;
		temp.setWidth(tWidth);
		fin>>asteroidCount;
		for(int j=0;j<asteroidCount;j++){
			int x=0,y=0;
			fin>>x>>y;
			temp.addAsteroid(x,y);
		}
		segmentList[i]=segment(temp);
	}
	fin.close();


	segmentQueue.push(segmentList[0]);
	segmentQueue.back().setInitialDist(0);
	segmentQueue.push(segmentList[0]);
	segmentQueue.back().setInitialDist(400);
	segmentQueue.push(segmentList[0]);
	segmentQueue.back().setInitialDist(800);
	segmentQueue.push(segmentList[0]);
	segmentQueue.back().setInitialDist(1200);
	segmentQueue.push(segmentList[0]);
	segmentQueue.back().setInitialDist(1600);
	queueMagnitude=2000;

	/*for (int i=0; i<BULLET_AMOUNT; i++)
		bulletQ.push(bulletStore[i]);*/
	frameCount = 0;

	start = clock();
	
}

//=============================================================================
// Destructor
//=============================================================================
Spacewar::~Spacewar()
{
    releaseAll();           // call onLostDevice() for every graphics item
}

//=============================================================================
// Initializes the game
// Throws GameError on error
//=============================================================================
void Spacewar::initialize(HWND hwnd)
{
    Game::initialize(hwnd); // throws GameError
	totalPixelDistance=0;

    // nebula texture
    if (!backTexture.initialize(graphics,BACK_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing nebula texture"));

	// background image
	if (!back.initialize(graphics, 2048, 1024, 0, &backTexture))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing nebula"));

	if (!asteroidTex.initialize(graphics,ASTEROID_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing asteroid texture"));

	for(int i=0;i<ASTEROID_AMOUNT;i++){
		if(!asteroidStore[i].initialize(this, 128, 128, 0, &asteroidTex)){
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing one of the asteroids"));
		}
	}

	if (!shipTex.initialize(graphics,SHIP_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player ship texture"));

	if (!pShip.initialize(this, 128, 64, 0, &shipTex))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing nebula"));

	// initialize bullet texture
	if (!bulletTex.initialize(graphics,BULLET_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing bullet texture"));

	//initialize bullet list
	for (int i=0; i<BULLET_AMOUNT; i++)
	{
		if(!bulletStore[i].initialize(this, 32, 16, 0, &bulletTex))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing one of the bullets"));
	}

	// init enemy ship texture
	if (!eShipTex.initialize(graphics,ENEMY_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing enemy ship texture"));

	// init enemy ship sprite
	if(!eShip.initialize(this, 128, 64, 0, &eShipTex)){
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing the enemy ship"));
	}

	// init damaged shield texture
	if (!shieldTex.initialize(graphics,DAMAGE_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing enemy damaged shield texture"));

	if(!sld.initialize(this, 128, 64, 0, &shieldTex)){
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player shield"));
	}

	if(!eSld.initialize(this, 128, 64, 0, &shieldTex)){
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing enemy shield"));
	}

	if (!phealth1.initialize(graphics,PHEALTH1_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player health bar texture"));

	if (!phealth2.initialize(graphics,PHEALTH2_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player health bar texture"));

	if (!phealth3.initialize(graphics,PHEALTH3_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player health bar texture"));

	if(!h1.initialize(this, 128, 32, 0, &phealth1)){
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player health"));
	}

	if(!h2.initialize(this, 128, 32, 0, &phealth2)){
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player health"));
	}

	if(!h3.initialize(this, 128, 32, 0, &phealth3)){
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player health"));
	}

	if (!eh1Tex.initialize(graphics,EHEALTH1_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing enemy health bar texture"));

	if (!eh2Tex.initialize(graphics,EHEALTH2_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing enemy health bar texture"));

	if (!eh3Tex.initialize(graphics,EHEALTH3_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing enemy health bar texture"));

	if (!eh4Tex.initialize(graphics,EHEALTH4_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing enemy health bar texture"));

	if (!eh5Tex.initialize(graphics,EHEALTH5_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing enemy health bar texture"));

	if (!eh6Tex.initialize(graphics,EHEALTH6_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing enemy health bar texture"));

	if (!eh7Tex.initialize(graphics,EHEALTH7_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing enemy health bar texture"));

	if(!eh1.initialize(this, 256, 32, 0, &eh1Tex)){
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing enemy health"));
	}

	if(!eh2.initialize(this, 256, 32, 0, &eh2Tex)){
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing enemy health"));
	}

	if(!eh3.initialize(this, 256, 32, 0, &eh3Tex)){
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing enemy health"));
	}

	if(!eh4.initialize(this, 256, 32, 0, &eh4Tex)){
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing enemy health"));
	}

	if(!eh5.initialize(this, 256, 32, 0, &eh5Tex)){
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing enemy health"));
	}

	if(!eh6.initialize(this, 256, 32, 0, &eh6Tex)){
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing enemy health"));
	}

	if(!eh7.initialize(this, 256, 32, 0, &eh7Tex)){
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing enemy health"));
	}

	if (!defeatTex.initialize(graphics,DEFEAT_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing game over screen texture"));

	if(!defeat.initialize(graphics, 1366, 768, 0, &defeatTex)){
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing game over screen"));
	}

	if (!victoryTex.initialize(graphics,VICTORY_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing VICTORY screen texture"));

	if(!victory.initialize(graphics, 1366, 768, 0, &victoryTex)){
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing victory screen"));
	}

	if(timerFont->initialize(graphics, 25, true, false, "Arial") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing DirectX font"));

	if(finalTimeFont->initialize(graphics, 40, true, false, "Arial") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing DirectX font"));

    return;
}

//=============================================================================
// Update all game items
//=============================================================================
void Spacewar::update()
{
   frameCount++;
	totalPixelDistance+=frameTime*levelVelocity;
	
	//if the first segment is done
	float v1=totalPixelDistance-segmentQueue.front().getInitialDist();
	float v2=segmentQueue.front().getWidth();
	if(totalPixelDistance>segmentQueue.front().getWidth()){
		totalPixelDistance=0;
		//spawn another one in
		queueMagnitude-=segmentQueue.front().getWidth();
		segmentQueue.pop();
		segment tempSegment=segment(segmentList[1]);
		if(rand()%2==1)tempSegment=segment(segmentList[2]);
		tempSegment.setInitialDist(totalPixelDistance);
		segmentQueue.push(tempSegment);
		for(int i=0;i<tempSegment.getNumAsteroids();i++){
			for(int j=0;j<ASTEROID_AMOUNT;j++){
				if(!asteroidStore[j].inUse){
					asteroidStore[j].spawn(tempSegment.getAsteroidX(i)+queueMagnitude,tempSegment.getAsteroidY(i));
					break;
				}
			}
		}
		
		queueMagnitude+=tempSegment.getWidth();
	}


	if (input->isKeyDown(VK_SPACE))
	{
		//b1.spawn(pShip.getX() + pShip.getWidth(), pShip.getY() + pShip.getHeight());
		if(!bulletFired) for (int i=0; i<BULLET_AMOUNT; i++)
		{
			if(!bulletStore[i].inUse)
			{
				//bulletQ.push(bulletStore[i]);
				bulletStore[i].spawn(pShip.getX() + SHIP_WEAPON_X, pShip.getY() + SHIP_WEAPON_Y);
				bulletStore[i].inUse = true;
				bulletFired=true;
				break;
			}

			
		}

	}
	else bulletFired=false;

	for (int i=0; i<BULLET_AMOUNT; i++)
	{
		if(bulletStore[i].getX() >= GAME_WIDTH)
			bulletStore[i].inUse = false;
		if(bulletStore[i].inUse)
			bulletStore[i].update(frameTime,BULLET_VELOCITY);
	}

	//scroll the asteroids
	for(int i=0;i<ASTEROID_AMOUNT;i++){
		if(asteroidStore[i].inUse) asteroidStore[i].update(frameTime,levelVelocity);
	}

	//update enemy ship
	eShip.update(frameTime);

	// move ship and detect border collisions
	if(input->isKeyDown(VK_UP)) 
	{
		if (pShip.getY() <= 0)
			pShip.setY(pShip.getY());
		else
			pShip.setY(pShip.getY()-SHIP_Y_VELOCITY*frameTime);
	}

	if(input->isKeyDown(VK_DOWN)) 
	{
		if (pShip.getY()  + pShip.getHeight() >= GAME_HEIGHT)
			pShip.setY(pShip.getY());
		else
			pShip.setY(pShip.getY()+SHIP_Y_VELOCITY*frameTime);
	}

	if(input->isKeyDown(VK_RIGHT))
	{
		if((levelVelocity+LEVEL_ACCEL_RATE*frameTime)<MAX_LEVEL_VELOCITY)
			levelVelocity+=LEVEL_ACCEL_RATE*frameTime;
	}

	if(input->isKeyDown(VK_LEFT))
	{
		if((levelVelocity-LEVEL_ACCEL_RATE*frameTime)>MIN_LEVEL_VELOCITY)
			levelVelocity-=LEVEL_ACCEL_RATE*frameTime;
	}

	pShip.setX(((levelVelocity-MIN_LEVEL_VELOCITY)/((MAX_LEVEL_VELOCITY-MIN_LEVEL_VELOCITY)))
		*(SHIP_MAX_X-SHIP_MIN_X)+SHIP_MIN_X);

	if(eShip.getState()==OFFSCREEN){
	eShipScreenTimer+=levelVelocity*frameTime;
	if(eShipScreenTimer>ESHIP_OFFSCREEN_DISTANCE){
		eShipScreenTimer=0;
		eShip.setState(ENTERING_SCREEN);
	}
	}

	// set shield positions	
	sld.setY(pShip.getY());
	sld.setX(pShip.getX());
	if(sld.engage){
		sld.startTime+=frameTime;
		if(sld.startTime>2) {
			sld.startTime=0;
			sld.engage=false;
		}
	}

	eSld.setY(eShip.getY());
	eSld.setX(eShip.getX());

	if (!isWon)
		elapsed = clock() - start;
}


//=============================================================================
// Artificial Intelligence
//=============================================================================
void Spacewar::ai()
{}

//=============================================================================
// Handle collisions
//=============================================================================
void Spacewar::collisions()
{
	    VECTOR2 collisionVector;
	for(int i=0;i<ASTEROID_AMOUNT;i++){
		if(!sld.engage&&asteroidStore[i].inUse&&!asteroidStore[i].collidedWith)
			if(pShip.collidesWith(asteroidStore[i], collisionVector)){
				OutputDebugStringA(to_string(i).c_str());
				asteroidStore[i].collidedWith=true;
				pShip.setHealth(pShip.getHealth()-1);
				//loss condition
				sld.engage = true;
				pShip.reduceHealth();
			}
	}

	/*for(int i=0;i<ASTEROID_AMOUNT;i++){
		if(eShip.getState()==ONSCREEN){
			if(eShip.collidesWith(asteroidStore[i], collisionVector))
				eShip.setShielded(true);
			else
				eShip.setShielded(false);
		}
	}*/

			
	for(int i=0;i<BULLET_AMOUNT;i++){
		if(bulletStore[i].inUse){
			for(int j=0;j<ASTEROID_AMOUNT;j++){
				if(asteroidStore[j].inUse){
					if(bulletStore[i].collidesWith(asteroidStore[j],collisionVector))
						bulletStore[i].inUse=false;
				}
			}
		}
	}

	// collision between bullets and enemy ship

	for (int i=0; i<BULLET_AMOUNT; i++)
	{
		if (bulletStore[i].inUse&&bulletStore[i].collidesWith(eShip, collisionVector)){
			bulletStore[i].inUse = false;
			if(eShip.getState()==ONSCREEN){
				numHits++;
				if (numHits % 2 ==0) // can adjust for difficulty
				{
					eShip.reduceHealth(); // every 5 hits takes a health point away from eShip
 					eShip.setState(DAMAGED);
				}
			}
		}
	}

    // if collision between ship and planet
    //if(ship1.collidesWith(planet, collisionVector))
    //{
    //    // bounce off planet
    //    ship1.bounce(collisionVector, planet);
    //    ship1.damage(PLANET);
    //}
    //if(ship2.collidesWith(planet, collisionVector))
    //{
    //    // bounce off planet
    //    ship2.bounce(collisionVector, planet);
    //    ship2.damage(PLANET);
    //}
    //// if collision between ships
    //if(ship1.collidesWith(ship2, collisionVector))
    //{
    //    // bounce off ship
    //    ship1.bounce(collisionVector, ship2);
    //    ship1.damage(SHIP);
    //    // change the direction of the collisionVector for ship2
    //    ship2.bounce(collisionVector*-1, ship1);
    //    ship2.damage(SHIP);
    //}
}

//=============================================================================
// Render game items
//=============================================================================
void Spacewar::render()
{
    graphics->spriteBegin();                // begin drawing sprites

    back.draw();                          // add the orion nebula to the scene

	timerFont->setFontColor(graphicsNS::WHITE);
	finalTimeFont->setFontColor(graphicsNS::RED);
	timerFont->print(to_string(((int)(double(elapsed) / CLOCKS_PER_SEC)) / 60)+":"+to_string((int)(double(elapsed) / CLOCKS_PER_SEC)), 600, 20);
	string saveTime = to_string(((int)(double(elapsed) / CLOCKS_PER_SEC)) / 60)+":"+to_string((int)(double(elapsed) / CLOCKS_PER_SEC));

	for(int i=0;i<ASTEROID_AMOUNT;i++){
		if(asteroidStore[i].inUse)asteroidStore[i].draw();
	}

	pShip.draw();
	eShip.draw();

	for(int i=0;i<BULLET_AMOUNT;i++)
	{
		if(bulletStore[i].inUse) bulletStore[i].draw();
	}
	
	if (sld.engage) sld.draw();
	

	if(eShip.isShielded()) eSld.draw();

	// draw player ship health
	if (pShip.getHealth() == 3 && !isWon)
		h3.draw();
	else if (pShip.getHealth() == 2 && !isWon)
		h2.draw();
	else if (pShip.getHealth() == 1 && !isWon)
		h1.draw();
	else if (!isWon)
	{
		defeat.draw();	//render defeat screen
		gameOver = true;
	}

	// draw enemy ship health
	if (eShip.getHealth() == 7 && !gameOver)
		eh7.draw();
	else if (eShip.getHealth() == 6 && !gameOver)
		eh6.draw();
	else if (eShip.getHealth() == 5 && !gameOver)
		eh5.draw();
	else if (eShip.getHealth() == 4 && !gameOver)
		eh4.draw();
	else if (eShip.getHealth() == 3 && !gameOver)
		eh3.draw();
	else if (eShip.getHealth() == 2 && !gameOver)
		eh2.draw();
	else if (eShip.getHealth() == 1 && !gameOver)
		eh1.draw();
	else if (!gameOver)
	{
		victory.draw();// render victory screen
		isWon = true;
		finalTimeFont->print("FINAL SCORE: "+saveTime, 500, 500);
	}

    graphics->spriteEnd();                  // end drawing sprites
}

//=============================================================================
// The graphics device was lost.
// Release all reserved video memory so graphics device may be reset.
//=============================================================================
void Spacewar::releaseAll()
{
    backTexture.onLostDevice();
    gameTextures.onLostDevice();
	asteroidTex.onLostDevice();
	shipTex.onLostDevice();
	eShipTex.onLostDevice();
	shieldTex.onLostDevice();
    Game::releaseAll();
    return;
}

//=============================================================================
// The grahics device has been reset.
// Recreate all surfaces.
//=============================================================================
void Spacewar::resetAll()
{
    gameTextures.onResetDevice();
    backTexture.onResetDevice();
	asteroidTex.onResetDevice();
	shipTex.onResetDevice();
	eShipTex.onResetDevice();
	shieldTex.onResetDevice();
    Game::resetAll();
    return;
}
