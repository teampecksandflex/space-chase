// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 ship.h v1.0

#ifndef _ASTEROID_H                 // Prevent multiple definitions if this 
#define _ASTEROID_H                 // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"

namespace asteroidNS
{
    const int WIDTH = 128;                   // image width
    const int HEIGHT = 128;                  // image height
    const int X = 0;
    const int Y = 0;
    const int   TEXTURE_COLS = 0;
}

// inherits from Entity class
class Asteroid : public Entity
{
public:
    // constructor
    Asteroid();

	float rotationRate;

    // inherited member functions
    virtual void draw();
    virtual bool initialize(Game *gamePtr, int width, int height, int ncols,
                            TextureManager *textureM);
    void update(float frameTime, float levelVelocity);
	void spawn(int x, int y);
	bool inUse;
	bool collidedWith;
};
#endif

