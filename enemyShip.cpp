#include "enemyShip.h"

//=============================================================================
// default constructor
//=============================================================================
enemyShip::enemyShip() : Entity()
{
    spriteData.width = enemyShipNS::WIDTH;           // size of Ship1
    spriteData.height = enemyShipNS::HEIGHT;
    spriteData.x = enemyShipNS::X;                   // location on screen
    spriteData.y = enemyShipNS::Y;
    spriteData.rect.bottom = enemyShipNS::HEIGHT;    // rectangle to select parts of an image
    spriteData.rect.right = enemyShipNS::WIDTH;
    radius = enemyShipNS::WIDTH/2.0;
    collisionType = entityNS::CIRCLE;
	health=ESHIP_INITIAL_HEALTH;
	direction=1;
	aiState=LEAVING_SCREEN;
	shielded=true;
	damageJitter=0;
}

//=============================================================================
// Initialize the Ship.
// Post: returns true if successful, false if failed
//=============================================================================
bool enemyShip::initialize(Game *gamePtr, int width, int height, int ncols,
    TextureManager *textureM)
{
    return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

//=============================================================================
// draw the ship
//=============================================================================
void enemyShip::draw()
{
    Image::draw();              // draw ship
}

state enemyShip::getState(){
	return aiState;
}

bool enemyShip::isShielded(){
	return shielded;
}
void enemyShip::setShielded(bool s){shielded=s;}

void enemyShip::setState(state s){
	aiState=s;
	timer=0;
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void enemyShip::update(float frameTime)
{
    Entity::update(frameTime);
	
	switch (aiState){
	case ONSCREEN:
		if(spriteData.y+direction*ESHIP_VELOCITY*frameTime<ESHIP_Y_BOUNDS 
			|| spriteData.y+direction*ESHIP_VELOCITY*frameTime>GAME_HEIGHT-ESHIP_Y_BOUNDS)direction=-direction;
		spriteData.y = (spriteData.y+direction*ESHIP_VELOCITY*frameTime);
		timer+=frameTime;
		/*if(timer>ESHIP_ONSCREEN_TIME) {
			aiState=LEAVING_SCREEN;
		}*/
		break;

	case DAMAGED:
		shielded=rand()%2;
		timer+=frameTime;
		spriteData.y-=damageJitter;
		damageJitter=(rand()%10)-5;
		spriteData.y+=damageJitter;
		if(timer>1) aiState=LEAVING_SCREEN;
		break;

	case LEAVING_SCREEN:
		shielded=true;
		spriteData.x = (spriteData.x+ESHIP_LEAVING_SPEED * frameTime);
		if(spriteData.x>GAME_WIDTH) {
			spriteData.y=(((rand()%(GAME_HEIGHT-2*ESHIP_Y_BOUNDS)) + ESHIP_Y_BOUNDS));
			timer=0;
			aiState=OFFSCREEN;
		}
		break;

	case ENTERING_SCREEN:
		spriteData.x =(spriteData.x-ESHIP_LEAVING_SPEED * frameTime);
		if(spriteData.x<ESHIP_X){
			timer=0;
			shielded=false;
			aiState=ONSCREEN;
		}
		break;

	case OFFSCREEN:
		break;
		/*timer+=frameTime;
		if(timer>5) aiState=ENTERING_SCREEN;*/
	}
}

int enemyShip::getHealth()
{
	return health;
}

void enemyShip::reduceHealth()
{
	health -= 1;
}

