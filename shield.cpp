#include "shield.h"

//=============================================================================
// default constructor
//=============================================================================
Shield::Shield() : Entity()
{
    spriteData.width = ShieldNS::WIDTH;           // size of Ship1
    spriteData.height = ShieldNS::HEIGHT;
    spriteData.x = ShieldNS::X;                   // location on screen
    spriteData.y = ShieldNS::Y;
    spriteData.rect.bottom = ShieldNS::HEIGHT;    // rectangle to select parts of an image
    spriteData.rect.right = ShieldNS::WIDTH;
    radius = ShieldNS::WIDTH/2.0;
    collisionType = entityNS::CIRCLE;
	health=SHIP_INITIAL_HEALTH;
	engage = false;
	startTime=0;
}

//=============================================================================
// Initialize the Ship.
// Post: returns true if successful, false if failed
//=============================================================================
bool Shield::initialize(Game *gamePtr, int width, int height, int ncols,
    TextureManager *textureM)
{
    return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

//=============================================================================
// draw the ship
//=============================================================================
void Shield::draw()
{
    Image::draw();              // draw ship
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void Shield::update(float frameTime)
{
    Entity::update(frameTime);
}