// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 constants.h v1.0

#ifndef _CONSTANTS_H            // Prevent multiple definitions if this 
#define _CONSTANTS_H            // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include <windows.h>

//-----------------------------------------------
// Useful macros
//-----------------------------------------------
// Safely delete pointer referenced item
#define SAFE_DELETE(ptr)       { if (ptr) { delete (ptr); (ptr)=NULL; } }
// Safely release pointer referenced item
#define SAFE_RELEASE(ptr)      { if(ptr) { (ptr)->Release(); (ptr)=NULL; } }
// Safely delete pointer referenced array
#define SAFE_DELETE_ARRAY(ptr) { if(ptr) { delete [](ptr); (ptr)=NULL; } }
// Safely call onLostDevice
#define SAFE_ON_LOST_DEVICE(ptr)    { if(ptr) { ptr->onLostDevice(); } }
// Safely call onResetDevice
#define SAFE_ON_RESET_DEVICE(ptr)   { if(ptr) { ptr->onResetDevice(); } }
#define TRANSCOLOR  SETCOLOR_ARGB(0,255,0,255)  // transparent color (magenta)


//-----------------------------------------------
//                  Constants
//-----------------------------------------------

// window
const char CLASS_NAME[] = "Spacewar";
const char GAME_TITLE[] = "Spacewar";
const bool FULLSCREEN = false;              // windowed or fullscreen
const UINT GAME_WIDTH =  1366;               // width of game in pixels
const UINT GAME_HEIGHT = 768;               // height of game in pixels

// game
const double PI = 3.14159265;
const float FRAME_RATE = 200.0f;                // the target frame rate (frames/sec)
const float MIN_FRAME_RATE = 10.0f;             // the minimum frame rate
const float MIN_FRAME_TIME = 1.0f/FRAME_RATE;   // minimum desired time for 1 frame
const float MAX_FRAME_TIME = 1.0f/MIN_FRAME_RATE; // maximum time used in calculations
const float GRAVITY = 6.67428e-11f;             // gravitational constant
const float MASS_PLANET = 1.0e14f;
const float MASS_SHIP = 5.0f;

const int ASTEROID_AMOUNT = 150;
const int SEGMENT_AMOUNT = 4;
const int NUM_SEGMENT_LANES = 8;
const int MIN_LEVEL_VELOCITY = 400;
const int MAX_LEVEL_VELOCITY = 900;
const int LEVEL_ACCEL_RATE = 500;

const int SHIP_INITIAL_HEALTH = 3;
const int SHIP_MIN_X = 100;
const int SHIP_MAX_X = 250;
const int SHIP_Y_VELOCITY = 700;
const int SHIP_WEAPON_X = 104;
const int SHIP_WEAPON_Y = 35;
const int BULLET_AMOUNT = 20;
const int BULLET_VELOCITY = 2500;

const int ESHIP_INITIAL_HEALTH = 7;
const int ESHIP_X = 1000;
const int ESHIP_INITIAL_X = 500;
const int ESHIP_VELOCITY = 150;
const int ESHIP_Y_BOUNDS = 100;
const int ESHIP_LEAVING_SPEED = 500;
const int ESHIP_ONSCREEN_TIME = 15;
const int ESHIP_OFFSCREEN_DISTANCE = 7000;


// graphic images
const char NEBULA_IMAGE[] =   "pictures\\orion.jpg";     // photo source NASA/courtesy of nasaimages.org 
const char BACK_IMAGE[] = "pictures\\Background.png";
const char TEXTURES_IMAGE[] = "pictures\\textures.png";  // game textures
const char ASTEROID_IMAGE[] = "pictures\\Asteroid_2.png";
const char SHIP_IMAGE[] = "pictures\\Spaceship1.png";
const char BULLET_IMAGE[] = "pictures\\Flame1.png";
const char ENEMY_IMAGE[] = "pictures\\Spaceship2.png";
const char DAMAGE_IMAGE[] = "pictures\\DamageShield.png";
const char PHEALTH1_IMAGE[] = "pictures\\Bar1.png";
const char PHEALTH2_IMAGE[] = "pictures\\Bar2.png";
const char PHEALTH3_IMAGE[] = "pictures\\Bar3.png";
const char EHEALTH1_IMAGE[] = "pictures\\eBar1.png";
const char EHEALTH2_IMAGE[] = "pictures\\eBar2.png";
const char EHEALTH3_IMAGE[] = "pictures\\eBar3.png";
const char EHEALTH4_IMAGE[] = "pictures\\eBar4.png";
const char EHEALTH5_IMAGE[] = "pictures\\eBar5.png";
const char EHEALTH6_IMAGE[] = "pictures\\eBar6.png";
const char EHEALTH7_IMAGE[] = "pictures\\eBar7.png";
const char DEFEAT_IMAGE[] = "pictures\\Defeat.png";
const char VICTORY_IMAGE[] = "pictures\\victory.png";

// key mappings
// In this game simple constants are used for key mappings. If variables were used
// it would be possible to save and restore key mappings from a data file.
const UCHAR ESC_KEY      = VK_ESCAPE;       // escape key
const UCHAR ALT_KEY      = VK_MENU;         // Alt key
const UCHAR ENTER_KEY    = VK_RETURN;       // Enter key


// weapon types
enum WEAPON {TORPEDO, SHIP, PLANET};

#endif
