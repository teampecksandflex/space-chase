#include "PHealth.h"

//=============================================================================
// default constructor
//=============================================================================
PHealth::PHealth() : Entity()
{
    spriteData.width = PHealthNS::WIDTH;           // size of Ship1
    spriteData.height = PHealthNS::HEIGHT;
    spriteData.x = PHealthNS::X;                   // location on screen
    spriteData.y = PHealthNS::Y;
    spriteData.rect.bottom = PHealthNS::HEIGHT;    // rectangle to select parts of an image
    spriteData.rect.right = PHealthNS::WIDTH;
    radius = PHealthNS::WIDTH/2.0;
    collisionType = entityNS::CIRCLE;
}

//=============================================================================
// Initialize the Ship.
// Post: returns true if successful, false if failed
//=============================================================================
bool PHealth::initialize(Game *gamePtr, int width, int height, int ncols,
    TextureManager *textureM)
{
    return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

//=============================================================================
// draw the ship
//=============================================================================
void PHealth::draw()
{
    Image::draw();              // draw ship
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void PHealth::update(float frameTime)
{
    Entity::update(frameTime);
}