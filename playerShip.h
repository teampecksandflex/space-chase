// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 ship.h v1.0

#ifndef _PSHIP_H                 // Prevent multiple definitions if this 
#define _PSHIP_H                 // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"

namespace playerShipNS
{
    const int WIDTH = 128;                   // image width
    const int HEIGHT = 64;                  // image height
    const int X = 100;
    const int Y = GAME_HEIGHT/2 - HEIGHT/2;
    const int   TEXTURE_COLS = 1;          
}

// inherits from Entity class
class playerShip : public Entity
{
private:
	int health;

public:
    // constructor
    playerShip();

    // inherited member functions
    virtual void draw();
    virtual bool initialize(Game *gamePtr, int width, int height, int ncols,
                            TextureManager *textureM);
    void update(float frameTime);

	int getHealth();
	void reduceHealth();
};
#endif

