#ifndef _SEGMENT_H             // Prevent multiple definitions if this 
#define _SEGMENT_H             // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "graphics.h"
#include "constants.h"
#include "spacewar.h"
#include <vector>

using namespace std;

class segment{
	string name;

	//lanes, width, initial pixel distance info (for stitching)
	//layout:
	// /lanes[0]--~--lanes[4]\
	// |lanes[1]--~--lanes[5]|
	// |lanes[2]--~--lanes[6]|
	// \lanes[3]--~--lanes[7]/
	//        <-width->
	unsigned int width;
	int initialDist;

	//"contents" of segments
	vector <VECTOR2> asteroidPositions;


public:
	bool lanes[NUM_SEGMENT_LANES];
	segment(string name, unsigned int tPD);
	segment();
	segment(const segment &s);
	void addAsteroid(int x, int y);
	void setLanes(bool laneVals[]);
	void setInitialDist(unsigned int d);
	void setWidth(int newWidth);
	void setName(string n);
	int getNumAsteroids();
	int getAsteroidX(int number);
	int getAsteroidY(int number);
	int getWidth();
	int getInitialDist();
	ostream& operator<<(ostream& o);
};

#endif