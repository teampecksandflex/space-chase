#include "spacewar.h"
#include "constants.h"
using namespace std;

segment::segment(string n, unsigned int tPD){
	name=n;
	for(int i=0;i<NUM_SEGMENT_LANES;i++) lanes[i]=false;
	width=0;
	initialDist = tPD;
}

segment::segment(){
	name="";
	for(int i=0;i<NUM_SEGMENT_LANES;i++) lanes[i]=false;
	width=0;
	initialDist = 0;
}

segment::segment(const segment &s){
	name=s.name;
	for(int i=0;i<NUM_SEGMENT_LANES;i++) lanes[i]=s.lanes[i];
	width=s.width;
	initialDist = s.initialDist;
	for(int i=0;i<s.asteroidPositions.size();i++)asteroidPositions.push_back(s.asteroidPositions[i]);
}

void segment::setInitialDist(unsigned int d){
	initialDist=d;
}

void segment::setName(string n){
	name=n;
}

void segment::addAsteroid(int x, int y){
	VECTOR2 input(x,y);
	asteroidPositions.push_back(input);
}

void segment::setLanes(bool laneVals[]){
	for(int i=0;i<NUM_SEGMENT_LANES;i++)
		lanes[i]=laneVals[i];
}

void segment::setWidth(int newWidth){width=newWidth;}

int segment::getNumAsteroids(){return asteroidPositions.size();}

int segment::getAsteroidX(int number){return asteroidPositions[number].x;}
int segment::getAsteroidY(int number){return asteroidPositions[number].y;}
int segment::getWidth(){return width;}
int segment::getInitialDist(){return initialDist;}
ostream& segment::operator<<(ostream& o){
	o<<name;
	return o;
}


