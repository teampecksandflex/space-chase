#ifndef _SHIELD_H                 // Prevent multiple definitions if this 
#define _SHIELD_H                 // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"

namespace ShieldNS
{
    const int WIDTH = 128;                   // image width
    const int HEIGHT = 64;                  // image height
    const int X = 1000;
    const int Y = GAME_HEIGHT/4 - HEIGHT/4;
    const int   TEXTURE_COLS = 1;          
}

// inherits from Entity class
class Shield : public Entity
{


public:
    // constructor
    Shield();
	float startTime;
    // inherited member functions
    virtual void draw();
    virtual bool initialize(Game *gamePtr, int width, int height, int ncols,
                            TextureManager *textureM);
    void update(float frameTime);

	bool engage;
};
#endif