// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 ship.cpp v1.0

#include "asteroid.h"

//=============================================================================
// default constructor
//=============================================================================
Asteroid::Asteroid() : Entity()
{
    spriteData.width = asteroidNS::WIDTH;           // size of Ship1
    spriteData.height = asteroidNS::HEIGHT;
    spriteData.x = asteroidNS::X;                   // location on screen
    spriteData.y = asteroidNS::Y;
    spriteData.rect.bottom = asteroidNS::HEIGHT;    // rectangle to select parts of an image
    spriteData.rect.right = asteroidNS::WIDTH;
    velocity.x = 0;                             // velocity X
    velocity.y = 0;                             // velocity Y
    radius = (asteroidNS::WIDTH/2.0)-20 ;
    collisionType = entityNS::CIRCLE;
	rotationRate = 0;
	inUse = false;
}

//=============================================================================
// Initialize the Ship.
// Post: returns true if successful, false if failed
//=============================================================================
bool Asteroid::initialize(Game *gamePtr, int width, int height, int ncols,
    TextureManager *textureM)
{
    return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

//=============================================================================
// draw the ship
//=============================================================================
void Asteroid::draw()
{
    Image::draw();              // draw asteroid
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void Asteroid::update(float frameTime, float levelVelocity)
{
    Entity::update(frameTime);
    spriteData.x -= frameTime * levelVelocity;         // move asteroid along X
	if(spriteData.x<-spriteData.width) inUse=false;
	spriteData.angle+=rotationRate*frameTime;
}

void Asteroid::spawn(int x, int y){
	spriteData.x=x;
	spriteData.y=y;
	rotationRate=((rand()%5)-2.5)/2.0;
	inUse=true;
	collidedWith=false;
}
