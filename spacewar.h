// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 spacewar.h v1.0

#ifndef _SPACEWAR_H             // Prevent multiple definitions if this 
#define _SPACEWAR_H             // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "game.h"
#include "textureManager.h"
#include "image.h"
#include "planet.h"
#include "ship.h"
#include "asteroid.h"
#include "segment.h"
#include "playerShip.h"
#include "enemyShip.h"
#include "bullet.h"
#include "shield.h"
#include "PHealth.h"
#include "EHealth.h"
#include "textDX.h"
#include <queue>
#include <ctime>

//=============================================================================
// This class is the core of the game
//=============================================================================
class Spacewar : public Game
{
private:
    // game items
	TextDX *timerFont;
	TextDX *finalTimeFont;
    TextureManager backTexture;   // nebula texture
    TextureManager gameTextures;    // game texture
	TextureManager asteroidTex;
	TextureManager shipTex;
	TextureManager bulletTex;
	TextureManager eShipTex;
	TextureManager shieldTex;
	TextureManager phealth1;
	TextureManager phealth2;
	TextureManager phealth3;
	TextureManager defeatTex, victoryTex;
	TextureManager eh1Tex, eh2Tex, eh3Tex, eh4Tex, eh5Tex, eh6Tex, eh7Tex;
	Shield sld, eSld;
	PHealth h1, h2, h3;
	EHealth eh1, eh2, eh3, eh4, eh5, eh6, eh7;
    //Ship    ship1, ship2;           // spaceships
    //Planet  planet;         // the planet
    Image   back;         // backdrop image
	Image defeat, victory;
	Asteroid asteroidStore[ASTEROID_AMOUNT];
	playerShip pShip;
	enemyShip eShip;
	segment segmentList[SEGMENT_AMOUNT];
	queue<segment> segmentQueue;
	unsigned int queueMagnitude;
	float eShipScreenTimer;
	Bullet bulletStore[BULLET_AMOUNT];
	queue<Bullet> bulletQ;
	bool gameOver, isWon;
	int numHits;
	clock_t start; // initialize with currecnt clock count
	clock_t elapsed;
public:
    // Constructor
    Spacewar();

    // Destructor
    virtual ~Spacewar();

    // Initialize the game
    void initialize(HWND hwnd);
    void update();      // must override pure virtual from Game
    void ai();          // "
    void collisions();  // "
    void render();      // "
    void releaseAll();
    void resetAll();
	float levelVelocity;
	float totalPixelDistance;
	float bulletVelocity;
	bool bulletFired;
	int frameCount;
};

#endif
